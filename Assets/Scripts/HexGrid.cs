﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class HexGrid : MonoBehaviour {

    public HexCell cellPrefab;
    public Text textPrefab;
    public int width = 10;
    public int height = 10;
    public float cellSize = 1;

    HexCell[] cells;
    Canvas canvas;

    void Awake()
    {
        canvas = GetComponentInChildren<Canvas>();
    }

	// Use this for initialization
	void Start ()
    {
        CreateGrid();
    }

    private void CreateGrid()
    {
        cells = new HexCell[width * height];
        int i = 0;
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                CreateCell(x, y, i++);
            }
        }
    }

    private void CreateCell(int x, int y, int i)
    {
        Vector3 pos = new Vector3()
        {
            x = (x + y * 0.5f - y / 2) * (HexMetrics.innerRadius * 2f),
            y = y * (HexMetrics.outerRadius * 1.5f),
            z = 0
        };

        HexCell cell = cells[i] = Instantiate<HexCell>(cellPrefab);
        cell.transform.SetParent(transform, false);
        cell.transform.localPosition = pos;

        cell.transform.Rotate(0, 180, UnityEngine.Random.Range(0,5)*60f);
        var hp = cell.GetComponent<HexPiece>();
        hp.outlineColor = Color.black;
        hp.showOutline = true;
    }

    // Update is called once per frame
    void Update () {
	    
	}
}
