﻿using UnityEngine;
using System.Collections;

public class HexPiece : MonoBehaviour {

    public Sprite front;
    public Sprite back;
    private Color _outlineColor;
    [SerializeField]
    public Color outlineColor {
        get { return _outlineColor; }
        set { _outlineColor = value; outlineRender.color = _outlineColor; }
    }
    private bool _showOutline;
    [SerializeField]
    public bool showOutline {
        get { return _showOutline; }
        set { _showOutline = value;outlineRender.gameObject.SetActive(_showOutline); }
    }
    public int sortLayerRelative = 0;

    private SpriteRenderer frontRender;
    private SpriteRenderer backRender;
    private SpriteRenderer outlineRender;

    void Awake()
    {
        frontRender = transform.FindChild("front").GetComponent<SpriteRenderer>();
        frontRender.sprite = front;

        backRender = transform.FindChild("back").GetComponent<SpriteRenderer>();
        backRender.sprite = back;

        outlineRender = transform.FindChild("outline").GetComponent<SpriteRenderer>();
        outlineRender.color = outlineColor;

        foreach(var a in GetComponentsInChildren<SpriteRenderer>())
        {
            a.sortingOrder += sortLayerRelative;
        }
    }
}
