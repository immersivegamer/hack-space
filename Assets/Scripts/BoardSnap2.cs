﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class BoardSnap2 : MonoBehaviour {

    public LayerMask attachToLayer;
    bool attach = false;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame    
    Vector2 pos;
    void Update()
    {
        if (attach)
        {
            pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = pos;            
        }
    }

    void OnMouseDown()
    {
        attach = true;        
    }

    void OnMouseUp()
    {
        attach = false;
        FindClosest();
    }

    void FindClosest()
    {
        var c2d = Physics2D.OverlapCircleAll(transform.position, 0.5f, attachToLayer.value);
        if (c2d.Length > 0)
        {
            var closest = c2d.OrderBy(x => Vector2.Distance(x.transform.position, this.transform.position)).First();
            this.transform.position = closest.transform.position;
        }
    }

    void OnDrawGizmos()
    {
        //Gizmos.color = Color.yellow;
        //Gizmos.DrawSphere(this.transform.position, 0.5f);
    }
}
