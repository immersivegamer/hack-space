﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;

public class UIScript : MonoBehaviour {

    [Header("EditBoard")]
    public Button editButton;
    public Image Icon;
    public Color On;
    public Color Off;

    [Header("DeployButtons")]
    public Button DeployA;
    public GameObject objA;
    public Button DeployB;
    public GameObject objB;
    public Button DeployC;
    public GameObject objC;
    public Button DeployD;
    public GameObject objD;


    void Start()
    {        
        editButton.onClick.AddListener(OnEditClick);
        DeployA.onClick.AddListener(() => ObjDeploy(objA));
        DeployB.onClick.AddListener(() => ObjDeploy(objB));
        DeployC.onClick.AddListener(() => ObjDeploy(objC));
        DeployD.onClick.AddListener(() => ObjDeploy(objD));
    }

	public void OnEditClick()
    {
        bool edit = !Board.canEdit;
        Board.canEdit = edit;
        Text text = Icon.GetComponentInChildren<Text>();
        text.text = edit ? "On" : "Off";
        Icon.color = edit ? On : Off;
    }

    public void ObjDeploy(GameObject prefab)
    {
        Vector3 center = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));
        center.z = -1;
        Instantiate(prefab, center, Quaternion.identity);
    }
}
