﻿using UnityEngine;
using System.Collections;

public class Board : MonoBehaviour {

    public static bool canEdit = true;
    static bool drag = false;
    public bool startsUp = false;
    bool up = false;

    Vector3 upRotate = new Vector3(0, 0, 0);
    Vector3 downRotate = new Vector3(0, 180, 0);
    static Vector3 sycnRotation = new Vector3(0, 0, 0);

    void Awake()
    {
        up = startsUp;
    }

    void OnMouseDown()
    {
        Flip();            
    }

    void OnMouseUp()
    {
        drag = false;
    }

    void OnMouseEnter()
    {
        if (drag)
            FlipSync();
    }    

    void Flip()
    {
        if (canEdit)
        {
            up = !up;
            transform.eulerAngles = up ? upRotate : downRotate;
            drag = true;
            sycnRotation = up ? upRotate : downRotate;
        }
    }

    void FlipSync()
    {
        transform.eulerAngles = sycnRotation;
    }
}
